package com.example.myapplication

import android.os.Bundle
import android.util.Log.d
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        randomNumberButton.setOnClickListener {
            d("Buttonclicked", "get number")
            val gotNumber = randomNumber()
            generatedNumber.text = gotNumber.toString()
            if (gotNumber > 0 && gotNumber % 5 == 0) {
                val also = "Yes".also { divisbleNumber.text = it }
            } else {
                "NO".also { divisbleNumber.text = it }
            }
        }

    }

    private fun randomNumber(): Int {
        return (-100..100).random()
    }


}